const path= require('path');
const HtmlWebPackPlugin      = require('html-webpack-plugin');
const MiniCssExtractPlugin   = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin= require('optimize-css-assets-webpack-plugin');
const CopyWebpackPlugin      = require('copy-webpack-plugin');
const MinifyPlugin           = require('babel-minify-webpack-plugin');
 const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports={
    //decimos que este en modo de desarrollo
    mode: 'production',

    //minimiza el css global
    optimization:{
        minimizer: [new OptimizeCssAssetsPlugin() ]
    },
    output:{
        filename:'main.[contenthash].js',
        path: path.resolve(__dirname, 'dist'),
    },
    //Cconfiguramos para colocar en el dist nuestro archivo html y css
    module:{
        rules:[
            {
                test:/\.css$/i,
                exclude:/Styles\.css$/i,
                 use:[
                     'style-loader',
                     'css-loader'
                 ]
            },
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: [
                   "babel-loader"
                ]
            },
            {
                test:/Styles\.css$/i,
                 use:[
                    MiniCssExtractPlugin.loader,
                     'css-loader'
                 ]
            },
            {
                test:/\.html$/i,
                loader: 'html-loader',
                options:{
                    attributes:false,
                    minimize:false, 
                },
            },
            {
                test:/\.(png|svg|jpg|gif)$/i,
                use:[
                    {
                        loader:'file-loader',
                        options:{
                            esModule:false,
                        }
                    }
                ],
            }
        ]
    },
    plugins:[
        new HtmlWebPackPlugin({
            template:'./src/index.html',
            filename :'./index.html'
        }),

        new MiniCssExtractPlugin({
            filename:'main.[contenthash].css',
            ignoreOrder: false,
        }),

        new CopyWebpackPlugin({
          patterns:[
            {from:'src/assets', to: 'assets/'}
          ],
        }),
        new MinifyPlugin(),
        new CleanWebpackPlugin(),
    ]
}